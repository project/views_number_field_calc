<?php

/**
 * @file
 * Definition of  views_number_field_calc_filter_group_by_numeric.
 */

class views_number_field_calc_filter_group_by_numeric extends views_handler_filter_group_by_numeric {

  /**
   * build query.
   */
  function query() {

    if (views_number_field_calc_detection($this->real_field)) {
      $info = $this->operators();

      $field = $this->options['selected_field'];
      $sql_code = $this->options['sql_code'];
      if (!empty($field) && !empty($info[$this->operator]['method'])) {
        if (trim($sql_code) != '') {
          $field = preg_replace('~\$field~', $field, $sql_code);
        }
        $this->{$info[$this->operator]['method']}($field);
      }
    }
    else {
      parent::query();
    }
  }

  /**
   * Define field form options.
   */
  function option_definition() {
    $options = parent::option_definition();
    if (views_number_field_calc_detection($this->real_field)) {
      $options['selected_field'] = array('default' => '');
      $options['sql_code'] = array('default' => '');
    }
    return $options;
  }

  /**
   * @access public
   * @param String $name
   * @return String
   */
  function _string_brackets($name) {
    return '(' . $name . ')';
  }

  function field_list($index = NULL) {
    $this->view->init_handlers();
    $this->view->init_query();
    $this->view->_build('field');

    $fields = array();
    $fields_array = $this->view->display_handler->get_handlers('field');
    foreach ($fields_array as $name => $field) {
      if (views_number_field_calc_detection($name)) {
        $field_name = $field->label() ? $field->label() : $name;
        $fields[$name] = $field->ui_name() . $this->_string_brackets($field_name);
      }
    }

    if ($index !== NULL) {
      return isset($fields[$index]);
    }

    return $fields;
  }

  function options_form(&$form, &$form_state) {
    if (views_number_field_calc_detection($this->real_field)) {
      $form['selected_field'] = array(
        '#type' => 'select',
        '#title' => t('Select Field'),
        '#options' => $this->field_list(),
        '#default_value' => $this->options['selected_field'],
        '#size' => 1,
      );
      $form['sql_code'] = array(
        '#type' => 'textarea',
        '#title' => t('Sql Code'),
        '#default_value' => $this->options['sql_code'],
        '#description' => t('$field is global'),
        '#size' => 1,
      );
    }
    parent::options_form($form, $form_state);
  }

}
