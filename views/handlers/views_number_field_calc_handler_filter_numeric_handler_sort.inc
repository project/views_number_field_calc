<?php

/**
 * @file
 * Definition of  views_number_field_calc_handler_filter_numeric_handler_sort.
 */

class views_number_field_calc_handler_filter_numeric_handler_sort extends views_handler_sort {

  function query() {

    $field = $this->options['selected_field'];
    if (!empty($field) && $this->field_list($field)) {
      $order = $this->options['order'];
      $this->query->orderby[] = array(
        'field' => $field,
        'direction' => strtoupper($order)
      );
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['selected_field'] = array('default' => '');
    return $options;
  }

  /**
   * @access public
   * @param String $name
   * @return String
   */
  function _string_brackets($name) {
    return '(' . $name . ')';
  }

  function field_list($index = NULL) {

    $this->view->init_handlers();
    $this->view->init_query();
    $this->view->_build('field');

    $fields = array();
    $fields_array = $this->view->display_handler->get_handlers('field');
    foreach ($fields_array as $name => $field) {
      if (views_number_field_calc_detection($name)) {
        $field_name = $field->label() ? $field->label() : $name;
        $fields[$name] = $field->ui_name() . $this->_string_brackets($field_name);
      }
    }

    if ($index !== NULL) {
      return isset($fields[$index]);
    }

    return $fields;
  }

  function options_form(&$form, &$form_state) {
    $form['selected_field'] = array(
      '#type' => 'select',
      '#title' => t('Select Field'),
      '#options' => $this->field_list(),
      '#default_value' => $this->options['selected_field'],
      '#size' => 1,
    );
    parent::options_form($form, $form_state);
  }

}
